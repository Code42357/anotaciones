
// Crear Carpetas
mkdir nombre1 nombre2...     
new-item nombre1 -type directory           

// Crear Archivos
new-item nombre1.txt

// Copiar Archivos
cp C:\nombre1.txt C:\Code\test
Copy-Item -Path C:\nombre1.txt -Destination C:\Code\test

// Mover Objetos
mv C:\nombre1.txt C:\Code\test
Copy-Item -Path C:\Code -Destination C:\Code\test

Nota: Para copiar y pegar, si son carpetas usar -recursive o -force segun el caso.

// Seleccionar Objetos
ls
get-childitem .
get-item .

Nota: get-childitem muestra el contenido de la carpeta mientras que get-item muestra la carpeta.

// Borrar Objetos
rm .
remove-item .
